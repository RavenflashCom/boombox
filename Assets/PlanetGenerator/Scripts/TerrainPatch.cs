﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LibNoise.Generator;
using System;

namespace PlanetGenerator
{
    public class TerrainPatch
    {
        Mesh mesh;
        int resolution;
        float offsetZ;
        TerrainSettingsSO settings;
        int seed;
        bool spherize;
        Vector3 forward;
        Vector3 up;
        Vector3 right;

        public TerrainPatch(TerrainSettingsSO settings, int seed, Mesh mesh, int resolution, Vector3 forward, float offsetZ = 0, bool spherize = false)
        {
            this.mesh = mesh;
            this.resolution = resolution;
            this.offsetZ = offsetZ;
            this.forward = forward;
            this.spherize = spherize;
            this.seed = seed;
            this.settings = settings;

            right = new Vector3(forward.y, forward.z, forward.x);
            up = Vector3.Cross(forward, right);
        }

        internal void RenderPlane(ComputeShader cs)
        {
            int vertsInLine = resolution + 1;
            Vector3[] verts = new Vector3[vertsInLine * vertsInLine];
            Vector2[] uvs = new Vector2[verts.Length];
            int[] tris = new int[6 * verts.Length];
            int trisId = 0;

            for (int y = 0; y < vertsInLine; y++)
            {
                for (int x = 0; x < vertsInLine; x++)
                {
                    int vertId = x + y * vertsInLine;
                    Vector2 pos = new Vector2(x, y) / resolution;
                    verts[vertId] = Position2DTo3D(pos);
                    uvs[vertId] = pos;

                    if (x >= resolution || y >= resolution) continue;

                    tris[trisId] = vertId;
                    tris[trisId + 1] = vertId + vertsInLine + 1;
                    tris[trisId + 2] = vertId + vertsInLine;

                    tris[trisId + 3] = vertId;
                    tris[trisId + 4] = vertId + 1;
                    tris[trisId + 5] = vertId + vertsInLine + 1;
                    trisId += 6;
                }
            }

            mesh.Clear();
            verts = GPURender(verts, cs);

            try
            {
                mesh.vertices = verts;
                mesh.triangles = tris;
                mesh.uv = uvs;
                mesh.RecalculateNormals();
            }
            catch (Exception e)
            {
                Debug.LogWarning("Incorrect data. Mesh could NOT be generated! " + e);
            }
        }

        private Vector3[] GPURender(Vector3[] verts, ComputeShader cs)
        {
            if(!cs)
            {
                Debug.LogError("Missing Terrain Shader");
                return verts;
            }
            if (!settings || settings.noiseLayers == null)
            {
                Debug.LogError("Noise Layer Data could not be found");
                return verts;
            }

            List<TerrainSettingsSO.NoiseSettings> ns = new List<TerrainSettingsSO.NoiseSettings>();
            foreach (TerrainSettingsSO.NoiseLayer layer in settings.noiseLayers) if (layer.enabled) ns.Add(layer.settings);
            TerrainSettingsSO.NoiseSettings[] nsa = ns.ToArray();
            if (nsa.Length <= 0) return verts;

            /////////////////////////////////////////////////////////////
            int kernel = cs.FindKernel("CalculateElevation");
            cs.SetFloat("amplitudeScale", settings.amplitudeScale);
            cs.SetBool("spherize", spherize);
            cs.SetInt("vertsCount", verts.Length);
            cs.SetInt("noiseCount", nsa.Length);
            cs.SetInt("seed", seed);
            

            ComputeBuffer vertBuffer = new ComputeBuffer(verts.Length, 12);
            vertBuffer.SetData(verts);
            cs.SetBuffer(kernel, "verts", vertBuffer);

            ComputeBuffer noiseBuffer = new ComputeBuffer(nsa.Length, 40);
            noiseBuffer.SetData(nsa);
            cs.SetBuffer(kernel, "noiseBuffer", noiseBuffer);


            cs.Dispatch(kernel, 1024, 1, 1);
            /////////////////////////////////////////////////////////////

            vertBuffer.GetData(verts);

            // Clean up
            vertBuffer.Dispose();
            noiseBuffer.Dispose();

            return verts;
        }

        private Vector3[] CPURender(Vector3[] verts)
        {

            return verts;
        }

        Vector3 Position2DTo3D(Vector2 pos)
        {
            float gameScaleRadius = (float)(settings.planetRadius * Planet.kilometerToMeter * Planet.planetaryObjScale);

            Vector3 v = forward * offsetZ + right * (pos.x - .5f) * 2 + up * (pos.y - .5f) * 2;
            if (spherize) v = v.normalized * gameScaleRadius;
            return v;
        }

        virtual protected Vector3 ApplyNoise(Vector3 v, Vector2 p, float scale, float freq)
        {
            return v;
        }


        public struct Crater
        {
            Vector3 center;
            float radius;

            public Crater(Vector3 center, float radius)
            {
                this.center = center;
                this.radius = radius;
            }

        }


    }
}