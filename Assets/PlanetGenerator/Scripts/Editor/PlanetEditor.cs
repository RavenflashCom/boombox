﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace PlanetGenerator
{
    [CustomEditor(typeof(Planet))]
    public class PlanetEditor : Editor
    {
        Planet planet;
        Editor dataEditor;

        public void OnEnable()
        {
            planet = (Planet)target;
        }

        override public void OnInspectorGUI()
        {
            planet.planetName = EditorGUILayout.TextField("Planet name", planet.planetName);
            planet.seed = EditorGUILayout.IntSlider("Seed", planet.seed, 1, 10000);
            planet.resolution = EditorGUILayout.IntSlider("Render resolution", planet.resolution, 1, 254);
            planet.useSingleMesh = EditorGUILayout.ToggleLeft("Use single mesh", planet.useSingleMesh);
            planet.hasWater = EditorGUILayout.ToggleLeft("Has water", planet.hasWater);
            planet.hasAtmosphere = EditorGUILayout.ToggleLeft("Has atmosphere", planet.hasAtmosphere);
            EditorGUILayout.Separator();
            planet.spin = EditorGUILayout.Vector3Field("Spin", planet.spin);
            EditorGUILayout.Separator();
            EditorGUILayout.BeginHorizontal();
            planet.autoUpdate = EditorGUILayout.ToggleLeft("Auto update", planet.autoUpdate, GUILayout.Width(100));
            
            if (GUILayout.Button("Render")) planet.Render();
            if (GUILayout.Button("Calculate Stats"))
            {
                planet.CalculatePlanetStats();
                GUI.changed = true;
            }
            EditorGUILayout.EndHorizontal();

            /////// Display Metric Statistics
            string msg = "\n";
            msg += "MAX Radius: " + planet.stats.maxRadius+ " km\n";
            msg += "MIN Radius: " + planet.stats.minRadius + " km";
            msg += (planet.hasWater)? " (water level)\n" : "\n";

            msg += "Avarage Radius: " + planet.stats.avgRadius + " km\n";
            if (planet.hasWater) msg += "MAX ocean depth: " + planet.stats.maxOceanDepth + " km\n";
            msg += "MAX Elevation: " + planet.stats.maxElevation + " km amsl \n";
            msg += "Total Mass: " + planet.stats.totalMass + " kg \n";
            EditorGUILayout.HelpBox(msg, MessageType.Info);
            EditorGUILayout.Separator();

            //////// Terrain Settings
            planet.terrainSettings = (TerrainSettingsSO)EditorGUILayout.ObjectField("Data", planet.terrainSettings, typeof(TerrainSettingsSO), false);
            if (planet.terrainSettings)
                planet.settingsFoldout = EditorGUILayout.InspectorTitlebar(planet.settingsFoldout, planet.terrainSettings);

            if (planet.settingsFoldout)
            {
                if (planet.terrainSettings)
                {
                    CreateCachedEditor(planet.terrainSettings, null, ref dataEditor);
                    dataEditor.OnInspectorGUI();
                }
            }
            ///////////////////////////

            if (planet.autoUpdate)
                if (GUI.changed)
                {
                    planet.Render();
                    planet.CalculatePlanetStats();
                }
            
            GUILayout.Space(50f);

        }
    }
}