﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace PlanetGenerator
{
    [CustomEditor(typeof(CombinedTerrain))]
    public class CombinedTerrainEditor : Editor
    {
        CombinedTerrain ct;
        Editor dataEditor;

        public void OnEnable()
        {
            ct = (CombinedTerrain)target;
        }

        override public void OnInspectorGUI()
        {
            ct.seed = EditorGUILayout.IntSlider("Seed", ct.seed, 1, 10000);
            ct.resolution = EditorGUILayout.IntSlider("Render resolution", ct.resolution, 1, 254);
            ct.useSingleMesh = EditorGUILayout.ToggleLeft("Use single mesh", ct.useSingleMesh);
            EditorGUILayout.Separator();
            EditorGUILayout.BeginHorizontal();
            ct.autoUpdate = EditorGUILayout.ToggleLeft("Auto update", ct.autoUpdate, GUILayout.Width(100));

            if (GUILayout.Button("Render")) ct.Render();
            EditorGUILayout.EndHorizontal();
            
            //////// Terrain Settings
            ct.terrainSettings = (TerrainSettingsSO)EditorGUILayout.ObjectField("Data", ct.terrainSettings, typeof(TerrainSettingsSO), false);
            if (ct.terrainSettings)
                ct.settingsFoldout = EditorGUILayout.InspectorTitlebar(ct.settingsFoldout, ct.terrainSettings);

            if (ct.settingsFoldout)
            {
                if (ct.terrainSettings)
                {
                    CreateCachedEditor(ct.terrainSettings, null, ref dataEditor);
                    dataEditor.OnInspectorGUI();
                }
            }
            ///////////////////////////

            if (ct.autoUpdate)
                if (GUI.changed)
                {
                    ct.Render();
                }

            GUILayout.Space(50f);

        }
    }
}