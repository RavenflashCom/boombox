﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace SpacePhysics
{
    [CustomEditor(typeof(GravityBehaviour))]
    public class GravityEditor : Editor
    {
        private GravityBehaviour gravity;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.Separator();

            gravity = (GravityBehaviour)target;

            //////////// TIME SIMULATION STEP SLIDER
            int timeStep = GravityBehaviour.timeStep;
            GravityBehaviour.timeStep = EditorGUILayout.IntSlider("Time step: ", GravityBehaviour.timeStep, 1, 1000);
            Time.timeScale = EditorGUILayout.Slider("Physix time multiplier: ", Time.timeScale, 1, 100);
            if(Time.timeScale > 10)
            {
                EditorGUILayout.HelpBox("WARNING! High speed physics simulation is very costly. FPS may drop drastically!", MessageType.Warning);
            }
            if (timeStep != Time.timeScale) GUI.changed = true;
            EditorGUILayout.Separator();
            /////////////////////////////////////////

            /// RESET LIST OF GRAVITY BODIES
            GravityBehaviour[] bodies = StarSystem.manager.GetAllCelestialBodies();
            if (bodies != null)
            {
                foreach (GravityBehaviour body in bodies)
                    EditorGUILayout.ObjectField(body, typeof(GravityBehaviour), true);
            }
            /////////////////////////////////////////

            if(GUI.changed && !Application.isPlaying)
            {
               gravity.InitRigidbody();
            }
        }

        public void OnSceneGUI()
        {
            GravityBehaviour gravity = (GravityBehaviour)target;
            if (gravity.drawOrbit) RenderPath();
            RenderArrow();
        }

        private void RenderPath()
        {
            GravityBehaviour gravity = (GravityBehaviour)target;

            if (!gravity) return;

            Handles.color = new Color(1f, 1f, .5f);

            Vector3[] trajectory = gravity.GetOrbit(64, 256);
            if (trajectory != null)
            {
                Vector3 v1, v2;
                for (int i = 0; i < trajectory.Length - 1; i++)
                {
                    v1 = trajectory[i];
                    v2 = trajectory[i + 1];
                    Handles.DrawLine(v1, v2);
                }

            }

        }

        private void RenderArrow()
        {
            GravityBehaviour gravity = (GravityBehaviour)target;
            if (gravity.velocity.magnitude <= 0) return;

            Handles.color = new Color(.4f, .5f, 1f);
            Handles.ArrowHandleCap(0, gravity.transform.position, Quaternion.LookRotation(gravity.velocity), gravity.velocity.magnitude*240f, EventType.Repaint);
        }



    }
}