﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using static PlanetGenerator.NoiseTextureGenerator;

namespace PlanetGenerator
{
    [CustomEditor(typeof(NoiseTextureGenerator))]
    public class TextureGeneratorEditor : Editor
    {

        NoiseTextureGenerator generator;

        public void OnEnable()
        {
            generator = (NoiseTextureGenerator)target;
        }

        override public void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if(GUILayout.Button("Render Texture"))
            {
                generator.Render((int)Mathf.Pow(2, (int)generator.resolution), 16);
            }

        }


    }
}