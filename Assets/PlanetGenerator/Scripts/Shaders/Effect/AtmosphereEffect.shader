﻿Shader "PlanetGenerator/AtmosphereShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}

    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
			#include "../Includes/MathHelper.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float4 viewVector: NORMAL;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
				/* https://docs.unity3d.com/ScriptReference/Camera-cameraToWorldMatrix.html */
				float3 viewVector = mul(unity_CameraInvProjection, float4(v.uv * 2 - 1, 0, -1));
				o.viewVector = mul(unity_CameraToWorld, float4(viewVector, 0));

				return o;
            }

            sampler2D _MainTex;
			sampler2D _CameraDepthTexture;
			float3 planetPosition;
			float planetRadius;
			float atmosphereRadius;
			float densityFalloff;
			float waterLevel;
			float3 dirToSun;
			int numOpticalDepthPoints;
			int numInScatteringPoints;
			float3 scatter;

			float densityAtPoint(float3 densitySamplePoint)
			{
				float heightAboveSurface = length(densitySamplePoint - planetPosition) - planetRadius;
				float height01 = heightAboveSurface / (atmosphereRadius - planetRadius);
				float localDensity = exp(-height01 * densityFalloff) *(1 - height01);
				return localDensity;
			}

			float opticalDepth(float3 rayOrigin, float3 rayDir, float rayLength)
			{
				float3 densitySamplePoint = rayOrigin;
				float stepSize = rayLength / (numOpticalDepthPoints - 1);
				float opticalDepth = 0;

				for (int i=0; i < numOpticalDepthPoints; i++)
				{
					float localDensity = densityAtPoint(densitySamplePoint);
					opticalDepth += localDensity * stepSize;
					densitySamplePoint += rayDir * stepSize;
				}

				return opticalDepth;
			}

			float3 calculateLight(float3 rayOrigin, float3 rayDir, float rayLength, float3 originalCol)
			{
				float3 inScatterPoint = rayOrigin;
				float stepSize = rayLength / (numInScatteringPoints - 1);
				float3 inScatteredLight = 0;
				float viewRayOpticalDepth = 0;

				for (int i = 0; i < numInScatteringPoints; i++) {
					float sunRayLength = raySphere(planetPosition, atmosphereRadius, inScatterPoint, dirToSun).y;
					float sunRayOpticalDepth = opticalDepth(inScatterPoint, dirToSun, sunRayLength);
					viewRayOpticalDepth = opticalDepth(inScatterPoint, -rayDir, stepSize * i);
					float3 transmittance = exp(-(sunRayOpticalDepth + viewRayOpticalDepth) * scatter);
					float localDensity = densityAtPoint(inScatterPoint);

					inScatteredLight += localDensity * transmittance* scatter * stepSize;
					inScatterPoint += rayDir * stepSize;
				}
				float originalColTransmittance = exp(-viewRayOpticalDepth);
				return inScatteredLight + originalCol * originalColTransmittance;
			}

            float4 frag (v2f i) : SV_Target
			{
				fixed4 originalCol = tex2D(_MainTex, i.uv);

				float3 rayPos = _WorldSpaceCameraPos;
				float viewLength = length(i.viewVector);
				float3 rayDir = i.viewVector / viewLength;

				float nonlin_depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, i.uv);
				float sceneDepth = LinearEyeDepth(nonlin_depth) * viewLength;

				float2 oceanHitInfo = raySphere(planetPosition, waterLevel, rayPos, rayDir);
				float dstToSurface = min(sceneDepth, oceanHitInfo.x);

				float2 hitInfo = raySphere(planetPosition, atmosphereRadius, rayPos, rayDir);
				float dstToAtmosphere = hitInfo.x;
				float dstThroughAtmosphere = hitInfo.y;

				// dst that view ray travels through ocean (before hitting terrain / exiting ocean)
				float atmosphereViewDepth = min(dstThroughAtmosphere, dstToSurface - dstToAtmosphere);
												
				////////////

				if (atmosphereViewDepth > 0) {
					const float snum = .00001;
					float3 pointInAtmosphere = rayPos + rayDir * (dstToAtmosphere+ snum);
					float3 light = calculateLight(pointInAtmosphere, rayDir, atmosphereViewDepth - snum*2, originalCol);
					
					return float4(light, 0);
					//return originalCol * (1 - light) + light;
				}

				return originalCol;
            }
            ENDCG
        }
    }
}
