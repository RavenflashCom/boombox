﻿Shader "PlanetGenerator/PlanetEffectsShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}

    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
			#include "../Includes/MathHelper.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float4 viewVector: NORMAL;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
				/* https://docs.unity3d.com/ScriptReference/Camera-cameraToWorldMatrix.html */
				float3 viewVector = mul(unity_CameraInvProjection, float4(v.uv * 2 - 1, 0, -1));
				o.viewVector = mul(unity_CameraToWorld, float4(viewVector, 0));

				return o;
            }

            sampler2D _MainTex;
			sampler2D _CameraDepthTexture;
			float3 planetPosition;
			float waterLevel; 
			float3 dirToSun;

			float4 colA;
			float4 colB;
			float4 specularCol;
			float depthMultiplier;
			float alphaMultiplier;
			float smoothness;

            float4 frag (v2f i) : SV_Target
			{
				fixed4 originalCol = tex2D(_MainTex, i.uv);

				float3 rayPos = _WorldSpaceCameraPos;
				float viewLength = length(i.viewVector);
				float3 rayDir = i.viewVector / viewLength;

				float nonlin_depth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, i.uv);
				float sceneDepth = LinearEyeDepth(nonlin_depth) * viewLength;

				float2 hitInfo = raySphere(planetPosition, waterLevel, rayPos, rayDir);
				float dstToOcean = hitInfo.x;
				float dstThroughOcean = hitInfo.y;
				float3 rayOceanIntersectPos = rayPos + rayDir * dstToOcean - planetPosition;

				// dst that view ray travels through ocean (before hitting terrain / exiting ocean)
				float oceanViewDepth = min(dstThroughOcean, sceneDepth - dstToOcean);

				float3 clipPlanePos = rayPos + i.viewVector * _ProjectionParams.y;
				float dstAboveWater = length(clipPlanePos - planetPosition) - waterLevel;

				if (oceanViewDepth > 0)
				{
					float4 c = 1 - exp(-oceanViewDepth * 16);
					float4 alpha = 1 - exp(-oceanViewDepth * 32);
					float4 oceanCol = lerp(colA, colB, c);
					float3 oceanNormal = normalize(rayOceanIntersectPos);

					////// DIFFUSE
					float diffuseLighting = saturate(dot(oceanNormal, dirToSun));
					oceanCol *= diffuseLighting;
					//////

					/*
					////// GAUSSIAN
					float specularAngle = acos(dot(normalize(dirToSun - rayDir), oceanNormal));
					float specularExp = specularAngle / (1-smoothness);
					float specularLight = exp(-specularExp * specularExp);
					oceanCol += specularLight * .6;
					//////
					*/

					////// BLINN
					float cosAngIncidence = dot(oceanNormal, dirToSun);
					cosAngIncidence = clamp(cosAngIncidence, 0, 1);
					float3 halfAngle = normalize(dirToSun - rayDir);
					float blinnTerm = dot(oceanNormal, halfAngle);

					blinnTerm = clamp(blinnTerm, 0, 1);
					blinnTerm = cosAngIncidence != 0.0 ? blinnTerm : 0.0;
					blinnTerm = pow(blinnTerm, smoothness * 100);
					if (dstAboveWater > 0) oceanCol += blinnTerm * .6;
					//////

					////// RIMLIGHT
					float rimLevel = .02f;
					float rimRatio = 1-dot(oceanNormal, -rayDir);
					float rimLight = pow(rimRatio, 1.45);
					oceanCol += rimLevel * rimLight * colB;
					//////

					return originalCol * (1 - alpha) + oceanCol * alpha;
				}
				return originalCol;
            }
            ENDCG
        }
    }
}
