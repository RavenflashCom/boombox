﻿// Each #kernel tells which function to compile; you can have many kernels
#pragma kernel CalculateElevation

// Create a RenderTexture with enableRandomWrite flag and set it
// with cs.SetTexture
RWTexture2D<float4> Result;
RWStructuredBuffer<float3> verts;
float amplitudeScale;
bool spherize;
uint vertsCount;
uint noiseCount;
int seed;

struct NoiseSettings {
	int noise;
	int mask;
	double frequency;
	float amplitude;
	float cropBottom;
	float cropTop;
	float3 noiseOffset;
};
StructuredBuffer<NoiseSettings> noiseBuffer;



// This implementation of Simplex Noise creates artifacts. Use only for 2D noise
//#include "../Includes/SimplexNoise.cginc"
// Below a better one is implemented
#include "../Includes/NoiseGenerator.cginc"
#include "../Includes/MathHelper.cginc"

[numthreads(64,1,1)]
void CalculateElevation(uint id : SV_DispatchThreadID)
{
	if (id >= vertsCount) { return; } 
	float3 v = verts[id];

	float a[3] = {0,0,0};
	
	for (uint i = 0; i < noiseCount; i++)
	{
		float noise = 0;
		NoiseSettings n = noiseBuffer[i];
		float3 s = float3(seed, seed, seed);
		float3 initialVector = (v + s + n.noiseOffset) * n.frequency;

		switch (n.noise)
		{
		case 1:
			noise += (1 + RegularNoise(initialVector)) / 2;
			break;
		case 2:
			noise += (1 + FractalNoise(initialVector)) / 2;
			break;
		case 3:
			noise += (1 + PeakNoise(initialVector)) / 2;
			break;
		case 4:
			noise += (1 + RidgedNoise(initialVector)) / 2;
			break;
		case 5:
			noise += (1 + CreaseNoise(initialVector)) / 2;
			break;
		case 6:
			noise += (1 + CraterNoise(initialVector)) / 2;
			break;

		default:
			break;
		}
		noise = max(n.cropBottom, noise) - n.cropBottom;
		noise = min(n.cropTop - n.cropBottom, noise);

		//0 - none, 1 - firstLayer, 2 - lastLayer, 3 - allLayers

		if (i > 0 && n.mask > 0 && n.mask <= 3) noise *= a[n.mask - 1];

		if (i == 0) a[0] = noise;
		a[1] = noise;
		noise *= n.amplitude;
		a[2] += noise;
		noise *= amplitudeScale;

		if(spherize)
			verts[id] += Normalize(v) * noise;
		else 
			verts[id] += float3(0,1,0) * noise;

	}
}
