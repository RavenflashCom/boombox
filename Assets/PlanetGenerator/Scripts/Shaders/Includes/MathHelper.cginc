﻿static const float maxFloat = 3.402823466e+38;

float3 Normalize(float3 v)
{
    return rsqrt(dot(v, v)) * v;
}

float Clamp01(float a)
{
    if (a < 0) return 0;
    else if (a > 1) return 1;
    else return a;
}

float SmoothMin(float a, float b, float k)
{
    float h = Clamp01((b - a + k) / (2 * k));
    return a * h + b * (1 - h) - k * h * (1 - h);
}

float SmoothMax(float a, float b, float k)
{
    return SmoothMin(a, b, -k);
}

float2 raySphere(float3 centre, float radius, float3 rayOrigin, float3 rayDir) {
	float3 offset = rayOrigin - centre;
	const float a = 1; // set to dot(rayDir, rayDir) instead if rayDir may not be normalized
	float b = 2 * dot(offset, rayDir);
	float c = dot(offset, offset) - radius * radius;

	float discriminant = b * b - 4 * a*c;
	// No intersections: discriminant < 0
	// 1 intersection: discriminant == 0
	// 2 intersections: discriminant > 0
	if (discriminant > 0) {
		float s = sqrt(discriminant);
		float dstToSphereNear = max(0, (-b - s) / (2 * a));
		float dstToSphereFar = (-b + s) / (2 * a);

		if (dstToSphereFar >= 0) {
			return float2(dstToSphereNear, dstToSphereFar - dstToSphereNear);
		}
	}
	// Ray did not intersect sphere
	return float2(maxFloat, 0);
}