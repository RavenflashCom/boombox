﻿Shader "PlanetGenerator/SimplePlanetShader"
{
    Properties
    {
        _MainTex ("Albedo", 2D) = "white" {}
		_Scale("Scale", float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
				//float2 uv : TEXCOORD0;
                float4 vertex : POSITION;
				float4 normal: NORMAL;
            };

            struct v2f
            {
                //float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float4 normal : NORMAL;
				float4 position: POSITIONT;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float _Scale;

            v2f vert (appdata v)
            {
                v2f o;
				o.position = v.vertex;
				o.vertex = UnityObjectToClipPos(v.vertex);
                //o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.normal = v.normal;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            float4 frag (v2f i) : SV_Target
            {
                // sample the texture
				// fixed4 col = tex2D(_MainTex, i.uv);
				float4 colX = tex2D(_MainTex, i.position.zy * _Scale);
				float4 colY = tex2D(_MainTex, i.position.xz * _Scale);
				float4 colZ = tex2D(_MainTex, i.position.xy * _Scale);
			
				float3 blend = i.normal * i.normal;
				blend = blend * blend;
				blend /= dot(blend, 1);

				float4 col = colX * blend.x + colY * blend.y + colZ * blend.z;
			
				// apply fog
				UNITY_APPLY_FOG(i.fogCoord, col);
				fixed lightShading = saturate(dot(i.normal, WorldSpaceLightDir(i.vertex).xyz));
				return col * lightShading;
            }
            ENDCG
        }
    }
}
