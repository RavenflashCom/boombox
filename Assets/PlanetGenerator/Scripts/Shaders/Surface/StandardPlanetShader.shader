﻿Shader "PlanetGenerator/StandardPlanetShader"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo", 2D) = "white" {}
		_BumpMap("Normal", 2D) = "bump" {}
		_BumpScale("Normal Map Size", Range(0,1)) = 1
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_Scale("Scale", float) = 1.0
	}
		SubShader
		{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma surface surf Standard vertex:vert fullforwardshadows

			//#pragma shader_feature _NORMALMAP
			//#pragma shader_feature _OCCLUSIONMAP

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

			sampler2D _MainTex;
			sampler2D _BumpMap;
			float _BumpScale;
			//float4 _MainTex_ST;
			half _Glossiness;
			half _Metallic;
			fixed4 _Color;
			float _Scale;

			struct Input
			{
				float2 uv_MainTex;
				float3 position;
				float3 localNormal;
			};

			// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
			// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
			// #pragma instancing_options assumeuniformscaling
			UNITY_INSTANCING_BUFFER_START(Props)
				// put more per-instance properties here
			UNITY_INSTANCING_BUFFER_END(Props)

			void vert(inout appdata_full v, out Input o)
			{
				UNITY_INITIALIZE_OUTPUT(Input, o);

				o.position = v.vertex.xyz;
				o.localNormal = v.normal.xyz;
			}

			void surf(Input IN, inout SurfaceOutputStandard o)
			{
				float3 pos = IN.position;

				float3 blend = normalize(abs(IN.localNormal));
				//blend = blend * blend;
				blend /= dot(blend, 1);

				float4 colX = tex2D(_MainTex, pos.zy * _Scale)* blend.x;
				float4 colY = tex2D(_MainTex, pos.xz * _Scale)* blend.y;
				float4 colZ = tex2D(_MainTex, pos.xy * _Scale)* blend.z;

				float4 col = (colX + colY + colZ) * _Color;

				// Albedo comes from a texture tinted by color
				// c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
				o.Albedo = col.rgb;
				o.Alpha = col.a;

				//#ifdef _NORMALMAP
				// Normal map
				half4 nx = tex2D(_BumpMap, pos.zy * _Scale) * blend.x;
				half4 ny = tex2D(_BumpMap, pos.xz * _Scale) * blend.y;
				half4 nz = tex2D(_BumpMap, pos.xy * _Scale) * blend.z;
				o.Normal = UnpackScaleNormal(nx + ny + nz, _BumpScale);
				//#endif

				/*
				#ifdef _OCCLUSIONMAP
				// Occlusion map
				half ox = tex2D(_OcclusionMap, tx).g * bf.x;
				half oy = tex2D(_OcclusionMap, ty).g * bf.y;
				half oz = tex2D(_OcclusionMap, tz).g * bf.z;
				o.Occlusion = lerp((half4)1, ox + oy + oz, _OcclusionStrength);
				#endif
				*/

				// Metallic and smoothness come from slider variables
				o.Metallic = _Metallic;
				o.Smoothness = _Glossiness;
			}
			ENDCG
		}
			FallBack "Diffuse"
}
