﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlanetGenerator
{
    [CreateAssetMenu(fileName = "ExampleTerrain", menuName = "Planet Generator/Terrain Settings", order = 2)]
    public class TerrainSettingsSO : ScriptableObject
    {
        public enum noiseType { none, regular, fractal, peak, ridged, crease, crater };
        public enum maskOption { none, firstLayer, lastLayer, allLayers };

        public double planetRadius = 1f;
        public float planetDensity = Mathf.Lerp( Planet.RockPlanetDensity.x, Planet.RockPlanetDensity.y, .5f);
        public double waterLevel = 1f;
        [Range(1f, 10f)]
        public float amplitudeScale = 1f;
        public NoiseLayer[] noiseLayers;

        [System.Serializable]
        public struct NoiseLayer
        {
            public string name;
            public bool enabled;
            [SerializeField]
            public NoiseSettings settings;
        }

        [System.Serializable]
        public struct NoiseSettings
        {
            public noiseType noise;
            public maskOption mask;
            [Range(0, 1f)]
            public double frequency;
            [Range(-1f, 1f)]
            public float amplitude;
            [Range(0, 1f)]
            public float cropBottom;
            [Range(0, 1f)]
            public float cropTop;
            public Vector3 noiseOffset;
        }

    }
}
