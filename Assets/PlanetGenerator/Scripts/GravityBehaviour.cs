﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlanetGenerator;
using System;

namespace SpacePhysics
{
    [RequireComponent(typeof(Planet))]
    public class GravityBehaviour : MonoBehaviour
    {
        public static int timeStep = 1000;

        public static readonly double G = 6.67430e-11;

        public Vector3 initialVelocity;
        public Vector3 velocity { get; private set; }
        public double gravityPower;



        [SerializeField]
        private GravityBehaviour orbitCenter = null;
        public bool drawOrbit = true;



        private Rigidbody rb;

        private void Init()
        {
            StarSystem.manager.Register(this);
            velocity = initialVelocity;

            Planet p = GetComponent<Planet>();

            gravityPower = (p.stats.totalMass * G * Math.Pow(Planet.planetaryObjScale, 3));

            // Rigidbody is optional
            if (!rb)
            {
                rb = GetComponent<Rigidbody>();
                InitRigidbody();
            }

        }


        private Vector3 GetGravityAcceleration(Vector3 position, GravityBehaviour otherBody)
        {
            Vector3 direction = otherBody.transform.position - position;

            double f = otherBody.gravityPower / direction.sqrMagnitude;
            Vector3 force = direction.normalized * (float)f; // force = acceleration
            return force;
        }

        private Vector3 GetGravityAcceleration(GravityBehaviour otherBody)
        {
            Vector3 v = GetGravityAcceleration(transform.position, otherBody);
            return v;
        }

        private void ApplyGravityFrom(GravityBehaviour gb)
        {
            Vector3 acceleration = GetGravityAcceleration(gb);
            //rb.AddForce(acceleration * Mathf.Pow(timeStep/2,2), ForceMode.Acceleration);
            rb.AddForce(acceleration * Mathf.Pow(timeStep, 2), ForceMode.Acceleration);
        }

        public void FixedUpdate()
        {
            // Do not perform FixedUpdate if there is no Rigidbody
            if (!rb) return;

            foreach (GravityBehaviour body in StarSystem.manager.CelestialBodies)
            {
                if (body != this) ApplyGravityFrom(body);
            }

        }

        private void Update()
        {
            // Perform only if there is no Rigidbody; otherwise FixedUpdate
            if (!rb)
            {
                UpdateVelocity();
                UpdatePosition();
            }
        }

        private void UpdateVelocity()
        {
            foreach (GravityBehaviour other in StarSystem.manager.GetAllCelestialBodies())
                if (other && other != this) velocity += GetGravityAcceleration(other) * timeStep * Time.deltaTime;
        }

        private void UpdatePosition()
        {
            transform.position += velocity * timeStep * Time.deltaTime; 
        }

        public void InitRigidbody()
        {
            if (rb)
            {
                rb.useGravity = false;
                rb.angularDrag = 0;
                rb.drag = 0;
                rb.mass = (float)gravityPower;
                rb.velocity = initialVelocity * timeStep;
            }
        }

        public Vector3[] GetOrbit(int renderEveryNth, int lines)
        {
            if (!orbitCenter) return null;
            List<Vector3> trajectory = new List<Vector3>();

            float timeScaleMultiplier = 100f / timeStep;

            Vector3 t = transform.position;
            trajectory.Add(t);
            Vector3 velocity = (rb)?rb.velocity/timeStep : this.velocity; /// timeStep;
            Vector3 acceleration;

            for (int j = 0; j < lines; j++)
            {
                for (int i = 0; i < renderEveryNth; i++)
                {
                    acceleration = GetGravityAcceleration(t, orbitCenter);
                    velocity += acceleration * timeStep * timeScaleMultiplier;
                    t += velocity * timeStep * timeScaleMultiplier;
                }
                trajectory.Add(t);
            }
            return trajectory.ToArray();
        }

        public void OnEnable()
        {
            Init();
        }

        public void OnValidate()
        {
            Init();
        }

        public void Start()
        {
            Init();
        }



    }
}
 