﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlanetGenerator
{
    [CreateAssetMenu(fileName = "AppSettings", menuName = "Planet Generator/Application Settings", order = 1)]
    public class AppSettingsSO : ScriptableObject
    {

        public ComputeShader terrainShader;

    }
}