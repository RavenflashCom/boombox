﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SpacePhysics
{
    public class StarSystem: MonoBehaviour
    {
        public List<GravityBehaviour> CelestialBodies
        {
            get {
                if (_CelestialBodies == null) _CelestialBodies = new List<GravityBehaviour>();
                return _CelestialBodies;
            }
        }

        private List<GravityBehaviour> _CelestialBodies;

        public static StarSystem manager {
            get {
                if (!_manager) _manager = FindObjectOfType<StarSystem>();
                return _manager;
            }
        }
        private static StarSystem _manager;

        void Awake()
        {
            _manager = this;
            _CelestialBodies = new List<GravityBehaviour>();
        }

        public void Register(GravityBehaviour body)
        {
            if(!CelestialBodies.Contains(body))
                CelestialBodies.Add(body);
        }

        public GravityBehaviour[] GetAllCelestialBodies()
        {
            return CelestialBodies.ToArray();
        }


    }
}