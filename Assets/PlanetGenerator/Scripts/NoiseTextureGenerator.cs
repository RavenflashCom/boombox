﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlanetGenerator
{
    public class NoiseTextureGenerator : MonoBehaviour
    {
        [SerializeField]
        ComputeShader compute = null;

        [SerializeField]
        public RenderTexture texture;

        public enum TextureResolution { x1, x2, x4, x8, x16, x32, x64, x128, x256, x512, x1024, x2048, x4096 };
        public TextureResolution resolution;

        [SerializeField]
        private FilterMode filterMode = FilterMode.Bilinear;

        [SerializeField]
        private bool checker = false;

        [SerializeField, Range(1, 8)]
        private int antiAliasing = 1;

        [SerializeField]
        internal Vector2 offset = Vector2.zero;


        public void Render(int resolution = 1024, int depth = 16)
        {
            texture = new RenderTexture(resolution, resolution, depth);
            texture.enableRandomWrite = true;
            texture.antiAliasing = antiAliasing;
            texture.filterMode = filterMode;
            texture.wrapMode = TextureWrapMode.Repeat;

            texture.Create();

            int kernel = compute.FindKernel("GetTexture");
            compute.SetBool("checker", checker);
            compute.SetInt("resolution", resolution);
            compute.SetFloat("offsetX", offset.x);
            compute.SetFloat("offsetY", offset.y);
            compute.SetTexture(kernel, "RenderTexture", texture);
            compute.Dispatch(kernel, resolution, resolution, 1);

            ApplyTexture();
        }

        private void ApplyTexture()
        {
            MeshRenderer[] renderers = GetComponentsInChildren<MeshRenderer>();
            Material mat;
            foreach (MeshRenderer r in renderers)
            {
                mat = r.sharedMaterial;
                if (mat) mat.SetTexture("_MainTex", texture);
            }

        }
    }
}