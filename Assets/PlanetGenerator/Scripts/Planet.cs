﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace PlanetGenerator
{
    [ExecuteInEditMode]
    public class Planet : CombinedTerrain
    {
        public static Vector2 RockPlanetDensity = new Vector2(3.5f, 6f); // g/cm3
        public static Vector2 GasGiantDensity = new Vector2(0.65f, 2f);
        public static readonly double planetaryObjScale = 10e-6; 
        public static readonly int kilometerToMeter = 1000;
        public static readonly int denstityGramToKgPerM3 = 1000;

        public string planetName = "";
        public bool hasWater = false;
        public bool hasAtmosphere = false;

        public Vector3 spin;

        public MetricData stats { get { return _stats; } }
        [SerializeField, HideInInspector]
        private MetricData _stats = new MetricData();

        Collider planetCollider;

        public void Start()
        {
            CalculatePlanetStats();
        }

        override protected void Init()
        {
            if (planetName == null || planetName.Length <= 0) planetName = name;
            
        }

        public void Update()
        {
            transform.Rotate(spin);
        }

        override protected void PrerenderInit()
        {
            patchesCount = 6;
            directions = new List<Vector3> { Vector3.up, Vector3.down, Vector3.right, Vector3.left, Vector3.forward, Vector3.back };
            base.PrerenderInit();
        }

        override protected TerrainPatch GetTerrainPatch(int i)
        {
            return new TerrainPatch(terrainSettings, seed, meshFilters[i].sharedMesh, combinedResolution, directions[i], 1f, true);
        }

        public void CalculatePlanetStats()
        {
            if (!terrainSettings) return;

            List<Vector3> verts = new List<Vector3>();
            _stats = new MetricData();

            foreach (MeshFilter m in meshFilters)
                verts.AddRange(m.sharedMesh.vertices);

            double groundElevation, oceanFloor;
            foreach (Vector3 v in verts)
            {
                double currentElevation = v.magnitude / planetaryObjScale / kilometerToMeter;

                groundElevation = (_stats.minRadius <= 0) ? currentElevation : Math.Min(_stats.minRadius, currentElevation);
                if (hasWater)
                {
                    oceanFloor = Math.Min(terrainSettings.waterLevel, groundElevation);
                    _stats.maxOceanDepth = Math.Max(_stats.maxOceanDepth, terrainSettings.waterLevel - oceanFloor);
                    _stats.maxRadius = Math.Max(Math.Max(_stats.maxRadius, currentElevation), terrainSettings.waterLevel);
                    _stats.minRadius = Math.Max(_stats.minRadius, terrainSettings.waterLevel);
                    _stats.avgRadius += Math.Max(currentElevation, terrainSettings.waterLevel) / verts.Count;
                }
                else
                {
                    _stats.minRadius = groundElevation;
                    _stats.maxRadius = Math.Max(_stats.maxRadius, currentElevation);
                    _stats.avgRadius += currentElevation / verts.Count;
                }

            }

            _stats.maxElevation = (hasWater) ? _stats.maxRadius - _stats.minRadius : _stats.maxRadius - _stats.avgRadius;

            double sphereVolume = 4f / 3f * Mathf.PI * (float)Math.Pow(_stats.avgRadius * kilometerToMeter, 3);
            _stats.totalMass = denstityGramToKgPerM3 * terrainSettings.planetDensity * sphereVolume;


            if (!planetCollider)
            {
                planetCollider = GetComponent<Collider>();
                if (!planetCollider)
                {
                    SphereCollider sphere = gameObject.AddComponent<SphereCollider>();
                    sphere.radius = (float)(stats.avgRadius * planetaryObjScale * kilometerToMeter);
                    planetCollider = sphere;
                }
            }

        }
    }

    [Serializable]
    public struct MetricData
    {
        public double minRadius;
        public double maxRadius;
        public double avgRadius;
        public double maxOceanDepth;
        public double maxElevation;
        public double totalMass;

        public MetricData(float min, float max, float avg, float maxOceanDepth, float maxElevation, float totalMass)
        {
            this.minRadius = min;
            this.maxRadius = max;
            this.avgRadius = avg;
            this.maxOceanDepth = maxOceanDepth;
            this.maxElevation = maxElevation;
            this.totalMass = totalMass;
        }

    }




}