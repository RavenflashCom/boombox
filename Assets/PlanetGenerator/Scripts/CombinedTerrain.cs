﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlanetGenerator
{
    [SelectionBase]
    public class CombinedTerrain : MonoBehaviour
    {
        [Range(1, 10000)]
        public int seed = 1;

        [Range(1, 255)]
        public int resolution = 1;
        public bool autoUpdate = true;
        public bool useSingleMesh = true;
        protected int combinedResolution;

        public TerrainSettingsSO terrainSettings;

        protected List<Vector3> directions = new List<Vector3> { Vector3.up };

        [SerializeField, HideInInspector]
        protected MeshFilter[] meshFilters;
        TerrainPatch[] terrainPatches;
        protected int patchesCount = 1;

        [HideInInspector]
        public bool settingsFoldout = false;

        virtual protected void Init()
        {
            //TODO: TO BE IMPLEMENTED IN THE FUTURE (maybe)
        }

        public void Render()
        {
            PrerenderInit();
            Generate();
        }

        virtual protected void PrerenderInit()
        {
            combinedResolution = (useSingleMesh) ? Mathf.FloorToInt(Mathf.Min(resolution, (255 / patchesCount))) : resolution;
            terrainPatches = new TerrainPatch[patchesCount];
            if (meshFilters == null || meshFilters.Length != patchesCount) meshFilters = new MeshFilter[patchesCount];

            for (int i = 0; i < patchesCount; i++)
            {
                if (meshFilters[i] == null)
                {
                    GameObject go = new GameObject("Mesh");
                    go.transform.parent = transform;
                    go.transform.localPosition = Vector3.zero;

                    MeshRenderer renderer = go.AddComponent<MeshRenderer>();
                    renderer.sharedMaterial = new Material(Shader.Find("Standard"));
                    meshFilters[i] = go.AddComponent<MeshFilter>();
                    meshFilters[i].mesh = new Mesh();
                }
                terrainPatches[i] = GetTerrainPatch(i);
                meshFilters[i].gameObject.SetActive(true);
            }
        }

        void Generate()
        {
            if (ReadyToRender())
            {
                foreach (TerrainPatch p in terrainPatches)
                {
                    p.RenderPlane(App.Settings.terrainShader);
                }

                MeshRenderer renderer = gameObject.GetComponent<MeshRenderer>();
                if (useSingleMesh) CombineMeshes();
                else if (renderer) renderer.enabled = false;
            }
        }

        virtual protected TerrainPatch GetTerrainPatch(int i)
        {
            return new TerrainPatch(terrainSettings, seed, meshFilters[i].sharedMesh, combinedResolution, directions[i], 0, false);
        }


        private bool ReadyToRender()
        {
            if (!App.Settings)
            {
                Debug.LogError("App Settings not found!");
                return false;
            }
            if (!App.Settings.terrainShader)
            {
                Debug.LogError("Missing Compute Shader. Cannot render " + name);
                return false;
            }
            if (!terrainSettings || terrainSettings.noiseLayers == null || terrainSettings.noiseLayers.Length == 0)
            {
                Debug.LogError("Missing Terrain Settings. Cannot render " + name);
                return false;
            }
            if (terrainSettings.planetRadius <= 0) return false;
            if (terrainSettings.waterLevel < 0) return false;

            return true;
        }

        private void CombineMeshes()
        {
            MeshFilter filter = gameObject.GetComponent<MeshFilter>();
            MeshRenderer renderer = gameObject.GetComponent<MeshRenderer>();
            if (!filter) filter = gameObject.AddComponent<MeshFilter>();
            if (!renderer) renderer = gameObject.AddComponent<MeshRenderer>();
            renderer.enabled = true;

            CombineInstance[] combine = new CombineInstance[meshFilters.Length];

            Vector3 p = transform.position;
            Vector3 s = transform.localScale;
            Quaternion r = transform.rotation;
            transform.position = Vector3.zero;
            transform.localScale = Vector3.one;
            transform.rotation = Quaternion.identity;

            for (int i = 0; i < meshFilters.Length; i++)
            {
                combine[i].mesh = meshFilters[i].sharedMesh;
                combine[i].transform = transform.localToWorldMatrix;
                meshFilters[i].gameObject.SetActive(false);
                renderer.sharedMaterial = meshFilters[i].GetComponent<MeshRenderer>().sharedMaterial;
            }

            if (filter.sharedMesh)
            {
                if (Application.isPlaying) Destroy(filter.sharedMesh);
                else DestroyImmediate(filter.sharedMesh);
            }

            filter.mesh = new Mesh();
            filter.sharedMesh.CombineMeshes(combine);

            transform.position = p;
            transform.rotation = r;
            transform.localScale = s;

        }

        public void Awake()
        {
            Init();
        }

        public void OnEnable()
        {
            Init();
        }

    }


}