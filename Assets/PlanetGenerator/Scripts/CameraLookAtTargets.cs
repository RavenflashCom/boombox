﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLookAtTargets : MonoBehaviour
{
    public List<Transform> FollowTargets = new List<Transform>();
    Camera cam;
    public bool stayInRange = true;
    public float range = 100f;

    public void Awake()
    {
        if(!cam) cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 t = Vector3.zero;

        for(int i=0; i<FollowTargets.Count; i++)
        {
            t += FollowTargets[i].position/FollowTargets.Count;
        }

        if (stayInRange)
        {
            Vector3 p = transform.position - t;
            p = t + p.normalized * range;

            transform.position = Vector3.Lerp(transform.position, p, .1f);

        }


        cam.transform.LookAt(t);


    }
}
