﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlanetGenerator {
    [ExecuteInEditMode]
    public class EffectManager : MonoBehaviour
    {
        private Planet[] planets;
        private Material waterEffect, atmosphereEffect;

        [SerializeField]
        private Light sunlight;


        public void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            Camera cam = GetComponent<Camera>();
            if (cam && waterEffect)
            {
                cam.depthTextureMode = DepthTextureMode.Depth;
                RenderTexture rt = RenderTexture.GetTemporary(source.width, source.height);
                Graphics.Blit(source, rt);

                foreach (Planet p in planets)
                {
                    if (p.hasWater)
                    {
                        SetWaterData(p);
                        RenderTexture rt2 = RenderTexture.GetTemporary(rt.width, rt.height);
                        Graphics.Blit(rt, rt2, waterEffect);
                        RenderTexture.ReleaseTemporary(rt);
                        rt = rt2;
                    }
                    if (p.hasAtmosphere) 
                    {
                        SetAtmosphereData(p);
                        RenderTexture rt2 = RenderTexture.GetTemporary(rt.width, rt.height);
                        Graphics.Blit(rt, rt2, atmosphereEffect);
                        RenderTexture.ReleaseTemporary(rt);
                        rt = rt2;
                    }
                }

                Graphics.Blit(rt, destination);
                RenderTexture.ReleaseTemporary(rt);
            }
        }

        private void Init()
        {
            waterEffect = new Material(Shader.Find("PlanetGenerator/PlanetEffectsShader"));
            atmosphereEffect = new Material(Shader.Find("PlanetGenerator/AtmosphereShader"));
            sunlight = FindObjectOfType<Light>();
            planets = FindObjectsOfType<Planet>();

        }

        private void SetWaterData(Planet p)
        {
            if (!p.terrainSettings) return;

            Vector3 planetPosition = p.transform.position;
            float waterLevel = (float)(p.terrainSettings.waterLevel * Planet.kilometerToMeter * Planet.planetaryObjScale);

            Vector4 colA = new Vector4(.4f, .7f, 1f, 1f);
            Vector4 colB = new Vector4(0, .1f, .2f, 1f);
            float smoothness = .85f;

            waterEffect.SetVector("planetPosition", planetPosition);
            waterEffect.SetFloat("waterLevel", waterLevel);
            waterEffect.SetVector("colA", colA);
            waterEffect.SetVector("colB", colB);
            waterEffect.SetFloat("smoothness", smoothness);
            waterEffect.SetVector("dirToSun", -sunlight.transform.forward);
        }

        private void SetAtmosphereData(Planet p)
        {
            Vector3 planetPosition = p.transform.position;
            float planetRadius = (float)(p.terrainSettings.planetRadius * Planet.kilometerToMeter * Planet.planetaryObjScale);
            float waterLevel = (float)(p.terrainSettings.waterLevel * Planet.kilometerToMeter * Planet.planetaryObjScale);

            float scatterStrength = 1f;
            float scatterR = Mathf.Pow(400f / 700f, 4) * scatterStrength;
            float scatterG = Mathf.Pow(400f / 530f, 4) * scatterStrength;
            float scatterB = Mathf.Pow(400f / 440f, 4) * scatterStrength;
            Vector3 scatter = new Vector3(scatterR, scatterG, scatterB);

            atmosphereEffect.SetVector("scatter", scatter);
            atmosphereEffect.SetVector("planetPosition", planetPosition);
            atmosphereEffect.SetFloat("planetRadius", planetRadius);
            atmosphereEffect.SetFloat("waterLevel", waterLevel);
            atmosphereEffect.SetFloat("atmosphereRadius", 80f);
            atmosphereEffect.SetFloat("densityFalloff", 24f);
            atmosphereEffect.SetInt("numOpticalDepthPoints", 32);
            atmosphereEffect.SetInt("numInScatteringPoints", 32);
            atmosphereEffect.SetVector("dirToSun", -sunlight.transform.forward);
            if (!p.terrainSettings) return;

        }

        void OnEnable()
        {
            Init();
        }

        void Awake()
        {
            Init();
        }

    }
}