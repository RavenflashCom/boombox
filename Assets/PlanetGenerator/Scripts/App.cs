﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlanetGenerator
{
    public class App : MonoBehaviour
    {
        public static AppSettingsSO Settings { get { return settings; } }
        private static AppSettingsSO settings;


        [SerializeField]
        private AppSettingsSO _settings = null;

        public void Awake()
        {
            Init();
        }

        public void OnValidate()
        {
            Init();
        }

        public void Init()
        {
            settings = this._settings;

        }

    }
}