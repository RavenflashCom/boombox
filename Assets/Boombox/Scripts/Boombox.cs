﻿using UnityEngine;
using PlanetGenerator;

namespace CodeSample
{
    public class Boombox : MonoBehaviour
    {
        public AudioClip musicFile;
        [Range(1, 10)]
        public float spectrumValueMultiplier = 10f;
        public bool useNewAudioSource = false;

        float spectrumValue = 0;
        float[] samples;
        AudioSource source;
        Planet boomboxSphere;
        float output = 0;

        // Start is called before the first frame update
        void Start()
        {
            samples = new float[128];
            source = GetAudioSource();

            boomboxSphere = GetComponent<Planet>();
            if(boomboxSphere)
                boomboxSphere.terrainSettings = Instantiate<TerrainSettingsSO>(boomboxSphere.terrainSettings);
        }

        // Update is called once per frame
        void Update()
        {
            if (!source.isPlaying) PlayMusic();

            if (boomboxSphere) ShapeBoomboxSphere();
            else ScaleBoomboxObject();
                
        }

        void ShapeBoomboxSphere()
        {
            if (!boomboxSphere.terrainSettings) {
                Debug.LogError("terrainSettings missing for boomboxSphere");
                return;
            }
            if (boomboxSphere.terrainSettings.noiseLayers == null)
            {
                Debug.LogError("noiseLayers Array missing for boomboxSphere.terrainSettings");
                return;
            }
            if (boomboxSphere.terrainSettings.noiseLayers.Length < 2)
            {
                Debug.LogError("noiseLayers Array for boomboxSphere.terrainSettings is to short");
                return;
            }

            AudioListener.GetSpectrumData(samples, 0, FFTWindow.Hamming);

            ShapeSpectrumLayer(0, 4, 1.2f);
            ShapeSpectrumLayer(1, 2, 1f);
            ShapeSpectrumLayer(2, 0, .2f);
            boomboxSphere.Render();
        }

        void ShapeSpectrumLayer(int layerId, int sample, float layerMultiplier = 1f)
        {
            if (boomboxSphere.terrainSettings.noiseLayers.Length < layerId + 1) return;
            if (samples != null && samples.Length >= sample)
            {
                spectrumValue = samples[sample] * spectrumValueMultiplier * layerMultiplier;
                boomboxSphere.terrainSettings.noiseLayers[layerId].settings.amplitude = Mathf.Lerp(boomboxSphere.terrainSettings.noiseLayers[layerId].settings.amplitude, Mathf.Round(spectrumValue * 100) / 100, .1f);
            }
        }

        void ScaleBoomboxObject()
        {
            AudioListener.GetSpectrumData(samples, 0, FFTWindow.Hamming);

            if (samples != null && samples.Length > 0)
            {
                spectrumValue = samples[0] * spectrumValueMultiplier;
                output = Mathf.Lerp(output, Mathf.Round(spectrumValue * 100) / 100, .1f);
            }

            transform.localScale = Vector3.one * (1 + output);
        }

        // Gets audiosource reference if one exits and useNewAudioSource is false. Otherwise creates one
        AudioSource GetAudioSource()
        {
            AudioSource a = gameObject.GetComponent<AudioSource>();
            if(!a || useNewAudioSource) a = gameObject.AddComponent<AudioSource>();
            return a;
        }

        // Plays music using musicFile
        void PlayMusic()
        {
            if (musicFile)
            {
                source.clip = musicFile;
                source.Play();
            }
            else
            {
                Debug.LogWarning("Music File not specified");
            }
        }
    }

}
